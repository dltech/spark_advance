#ifndef H_WATCHDOG_REG
#define H_WATCHDOG_REG
/*
 * STM8 basic support library. Register definitions of window watchdog (WWDG),
 * independed watchdog (IWDG) and reset modules.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Reset status register */
#define RST_SR      MMIO8(RST_BASE)
// EMC reset flag
#define EMCF    0x10
// SWIM reset flag
#define SWIMF   0x08
// illegal opcode reset flag
#define ILLOPF  0x04
// Independent Watchdog reset flag
#define IWDGF   0x02
// Window Watchdog reset flag
#define WWDGF   0x01

/* Key register */
#define IWDG_KR     MMIO8(IWDG_BASE + 0x00)
#define KEY_ENABLE  0xcc
#define KEY_REFRESH 0xaa
#define KEY_ACCESS  0x55

/* Prescaler register */
#define IWDG_PR     MMIO8(IWDG_BASE + 0x01)
// Prescaler divider
#define IWDG_DIV4   0x0
#define IWDG_DIV8   0x1
#define IWDG_DIV16  0x2
#define IWDG_DIV32  0x3
#define IWDG_DIV64  0x4
#define IWDG_DIV128 0x5
#define IWDG_DIV256 0x6

/* Reload register */
#define IWDG_RLR    MMIO8(IWDG_BASE + 0x02)
// Watchdog counter reload value RL[7:0]

/* Control register */
#define WWDG_CR     MMIO8(WWDG_BASE + 0x00)
// Activation bit
#define WDGA            0x80
// 7-bit counter (MSB to LSB)
#define WDG_T_MSK       0x7f
#define WDG_T_GET       (WWDG_CR*WDG_T_MSK)
#define WDG_T_SET(n)    (n&WDG_T_MSK)

/* Window register */
#define WWDG_WR     MMIO8(WWDG_BASE + 0x01)
// 7-bit window value

#endif
