#ifndef H_FLASH_REG
#define H_FLASH_REG
/*
 * STM8 basic support library. STM8 eeprom/flash register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Flash control register 1 */
#define FLASH_CR1   MMIO8(FLASH_BASE + 0x00)
// Power-down in Halt mode
#define HALT    0x08
// Power-down in Active-halt mode
#define AHALT   0x04
// Flash Interrupt enable
#define IE      0x02
// Fixed Byte programming time
#define FIX     0x01

/* Flash control register 2 */
#define FLASH_CR2   MMIO8(FLASH_BASE + 0x01)
// Write option bytes
#define OPT     0x80
// Word programming
#define WPRG    0x40
// Block erasing
#define ERASE   0x20
// Fast block programming
#define FPRG    0x10
// Standard block programming
#define PRG     0x01

/* Flash complementary control register 2 */
#define FLASH_NCR2  MMIO8(FLASH_BASE + 0x02)
// Write option bytes
#define NOPT    0x80
// Word programming
#define NWPRG   0x40
// Block erase
#define NERASE  0x20
// Fast block programming
#define NFPRG   0x10
// Block programming
#define NPRG    0x01

/* Flash protection register */
#define FLASH_FPR   MMIO8(FLASH_BASE + 0x03)
// User boot code area protection bits
#define WPB5    0x20
#define WPB4    0x10
#define WPB3    0x08
#define WPB2    0x04
#define WPB1    0x02
#define WPB0    0x01

/* Flash protection register */
#define FLASH_NFPR  MMIO8(FLASH_BASE + 0x04)
// User boot code area protection bits
#define NWPB5   0x20
#define NWPB4   0x10
#define NWPB3   0x08
#define NWPB2   0x04
#define NWPB1   0x02
#define NWPB0   0x01

/* Flash status register */
#define FLASH_IAPSR MMIO8(FLASH_BASE + 0x05)
// End of high voltage flag
#define HVOFF       0x40
// Data EEPROM area unlocked flag
#define DUL         0x08
// End of programming (write or erase operation) flag
#define EOP         0x04
// Flash Program memory unlocked flag
#define PUL         0x02
// Write attempted to protected page flag
#define WR_PG_DIS   0x01

/* Flash program memory unprotecting key register */
#define FLASH_PUKR  MMIO8(FLASH_BASE + 0x08)
// Main program memory unlock keys

/* Data EEPROM unprotection key register */
#define FLASH_DUKR  MMIO8(FLASH_BASE + 0x0a)
// Data EEPROM write unlock keys

#define FIRST_DATA_KEY  0xae
#define SECOND_DATA_KEY 0x56

/* Read-out protection (ROP) */
#define OPT0_ADDR   (OPTION_START + 0x00)
#define OPT0        MMIO8(OPTION_START + 0x00)
// Memory readout protection
#define ROP_EN_PROT 0xaa

/* User boot code (UBC) */
#define OPT1_ADDR   (OPTION_START + 0x01)
#define OPT1        MMIO8(OPTION_START + 0x01)
#define NOPT1       MMIO8(OPTION_START + 0x02)
// User boot code area
#define NO_UBC          0x00
#define UBC_PAGE0       0x01
#define UBC_PAGE0TO1    0x02
#define UBC_PAGE0TON(n) (n&0x7f)

/* Alternate function remapping (AFR) */
#define OPT2_ADDR   (OPTION_START + 0x03)
#define OPT2        MMIO8(OPTION_START + 0x03)
#define NOPT2       MMIO8(OPTION_START + 0x04)
// Alternate function remapping option 7
#define AFR7    0x80
// Alternate function remapping option 6
#define AFR6    0x40
// Alternate function remapping option 5
#define AFR5    0x20
// Alternate function remapping option 4
#define AFR4    0x10
// Alternate function remapping option 3
#define AFR3    0x08
// Alternate function remapping option 2
#define AFR2    0x04
// Alternate function remapping option 1
#define AFR1    0x02
// Alternate function remapping option 0
#define AFR0    0x01

/* Misc. option */
#define OPT3_ADDR   (OPTION_START + 0x05)
#define OPT3        MMIO8(OPTION_START + 0x05)
#define NOPT3       MMIO8(OPTION_START + 0x06)
// high-speed internal clock trimming register size
#define HSITRIM         0x10
// Low speed internal clock enable
#define LSI_EN          0x08
// Independent watchdog
#define IWDG_HW         0x04
// Window watchdog activation
#define WWDG_HW         0x02
// Window watchdog reset on halt
#define WWDG_HALT       0x01

/* Clock option */
#define OPT4_ADDR   (OPTION_START + 0x07)
#define OPT4        MMIO8(OPTION_START + 0x07)
#define NOPT4       MMIO8(OPTION_START + 0x08)
// External clock selection
#define EXTCLK          0x08
// Auto wakeup unit/clock
#define CKAWUSEL        0x04
// AWU clock prescaler
#define PRSC16M_128K    0x00
#define PRSC8M_128K     0x02
#define PRSC4M_128K     0x03

/* HSE clock startup */
#define OPT5_ADDR   (OPTION_START + 0x09)
#define OPT5        MMIO8(OPTION_START + 0x09)
#define NOPT5       MMIO8(OPTION_START + 0x0a)
// HSE crystal oscillator stabilization time
#define HSE2048 0x00
#define HSE128  0xb4
#define HSE8    0xd2
#define HSE0P5  0xe1

#endif
