#ifndef H_MEMORYMAP
#define H_MEMORYMAP
/*
 * STM8 basic support library. STM8S003F3 memory addresses.
 *
 * Copyright 2022 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define  MMIO8(addr)		(*(volatile uint8_t *)(addr))

#define PORTA_BASE  0x5000
#define PORTB_BASE  0x5005
#define PORTC_BASE  0x500a
#define PORTD_BASE  0x500f
#define PORTE_BASE  0x5014
#define PORTF_BASE  0x5019
#define FLASH_BASE  0x505a
#define EXTI_BASE   0x50a0
#define RST_BASE    0x50b3
#define CLK_BASE    0x50c0
#define WWDG_BASE   0x50d1
#define IWDG_BASE   0x50e0
#define AWU_BASE    0x50f0
#define BEEP_BASE   0x50f3
#define SPI_BASE    0x5200
#define I2C_BASE    0x5210
#define UART1_BASE  0x5230
#define TIM1_BASE   0x5250
#define TIM2_BASE   0x5300
#define TIM4_BASE   0x5340
#define ADC1_BASE   0x53e0
#define CPU_BASE    0x7f00
#define ITC_BASE    0x7f70
#define SWIM_BASE   0x7f80
#define DM_BASE     0x7f90

#define EEPROM_START    0x4000
#define EEPROM_END      0x427f

#define OPTION_START    0x4800
#define OPTION_END      0x483f

#define FLASH_START     0x8080
#define FLASH_END       0x9fff

#endif
