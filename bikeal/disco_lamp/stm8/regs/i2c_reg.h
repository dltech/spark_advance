#ifndef H_I2C_REG
#define H_I2C_REG
/*
 * STM8 basic support library. I2C register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"


/* Control register 1 */
#define I2C_CR1     MMIO8(I2C_BASE + 0x00)
// Clock stretching disable
#define NOSTRETCH   0x80
// General call enable
#define ENGC        0x40
// Peripheral enable
#define PE          0x01

/* Control register 2 */
#define I2C_CR2     MMIO8(I2C_BASE + 0x01)
// Software reset
#define SWRST   0x80
// Acknowledge position
#define POS     0x08
// Acknowledge enable
#define ACK     0x04
// Stop generation
#define STOP    0x02
// Start generation
#define START   0x01

/* Frequency register */
#define I2C_FREQR   MMIO8(I2C_BASE + 0x02)
// Peripheral clock frequency.
#define FREQ_MSK    0x3f

/* Own address register LSB */
#define I2C_OARL    MMIO8(I2C_BASE + 0x03)
// Interface address [7:0]

/* Own address register MSB */
#define I2C_OARH    MMIO8(I2C_BASE + 0x04)
// Addressing mode
#define ADDMODE         0x80
// Address mode configuration
#define ADDCONF         0x40
// Interface address
#define ADD9_8_MSK      0x06
#define ADD9_8_SHIFT    1

/* Data register */
#define I2C_DR      MMIO8(I2C_BASE + 0x06)
// Byte received or to be transmitted to the bus.

/* Status register 1 */
#define I2C_SR1     MMIO8(I2C_BASE + 0x07)
// Data register empty
#define TXE     0x80
// Data register not empty
#define RXNE    0x40
// Stop detection
#define STOPF   0x10
// 10-bit header sent
#define ADD10   0x08
// Byte transfer finished
#define BTF     0x04
// Address sent (master mode)/matched (slave mode)
#define ADDR    0x02
// Start bit
#define SB      0x01

/* Status register 2 */
#define I2C_SR2     MMIO8(I2C_BASE + 0x08)
// Wakeup from Halt.
#define WUFH    0x20
// Overrun/underrun.
#define OVR     0x08
// Acknowledge failure.
#define AF      0x04
// Arbitration lost.
#define ARLO    0x02
// Bus error.
#define BERR    0x01

/* Status register 3 */
#define I2C_SR3     MMIO8(I2C_BASE + 0x09)
// Dual flag (Slave mode)
#define DUALF   0x80
// General call header (Slave mode)
#define GENCALL 0x10
// Transmitter/Receiver
#define TRA     0x04
// Bus busy
#define BUSY    0x02
// Master/Slave
#define MSL     0x01

/* Interrupt register */
#define I2C_ITR     MMIO8(I2C_BASE + 0x0a)
// Buffer interrupt enable
#define ITBUFEN 0x04
// Event interrupt enable
#define ITEVTEN 0x02
// Error interrupt enable
#define ITERREN 0x01

/* Clock control register low */
#define I2C_CCRL    MMIO8(I2C_BASE + 0x0b)
// SCLH clock in Master mode

/* Clock control register high */
#define I2C_CCRH    MMIO8(I2C_BASE + 0x0c)
// I2C master mode selection
#define F_S         0x80
// Fast mode duty cycle
#define DUTY        0x40
// Clock control register in Fast/Standard mode
#define CCR_MSK     0x0f

/* TRISE register */
#define I2C_TRISER  MMIO8(I2C_BASE + 0x0d)
// Maximum rise time in Fast/Standard mode
#define TRISE_MSK   0x3f


#endif
