#ifndef H_AWU_REG
#define H_AWU_REG
/*
 * STM8 basic support library. Auto wake up from halt register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"


/* Control/status register */
#define AWU_CSR    MMIO8(AWU_BASE + 0x00)
// Auto-wakeup flag
#define AWUF    0x20
// Auto-wakeup enable
#define AWUEN   0x10
// Measurement enable
#define MSR     0x01

/* Asynchronous prescaler register */
#define AWU_APR    MMIO8(AWU_BASE + 0x01)
// prescaler divider (APRDIV) feeding the counter clock
#define APR(n)  (n-2)
#define APR_MSK 0x3f

/* Timebase selection register */
#define AWU_TBR    MMIO8(AWU_BASE + 0x02)
// the time interval between AWU interrupts.
#define AWUTB_NOIT      0x01
#define AWUTB_MUL2      0x02
#define AWUTB_MUL4      0x03
#define AWUTB_MUL8      0x04
#define AWUTB_MUL16     0x05
#define AWUTB_MUL32     0x06
#define AWUTB_MUL64     0x07
#define AWUTB_MUL128    0x08
#define AWUTB_MUL256    0x09
#define AWUTB_MUL512    0x0a
#define AWUTB_MUL1024   0x0b
#define AWUTB_MUL2048   0x0c
#define AWUTB_MUL4096   0x0d
#define AWUTB_MUL10240  0x0e
#define AWUTB_MUL61440  0x0f


#endif
