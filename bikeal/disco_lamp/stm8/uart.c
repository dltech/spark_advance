/*
 * STM8 basic support library. UART standard functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "uart.h"

void uartMinimalInit()
{
    portConfig(UART_PORT, UART_TX_PIN, OUTPUT_PP_10M);
    portConfig(UART_PORT, UART_RX_PIN, INPUT_PULLUP);
    enable(UART1);
    UART1_CR1 = 0;
    UART1_CR2 = TEN | REN | RIEN;
    UART1_CR3 = STOP_2;
    UART1_CR4 = 0;
    UART1_CR5 = 0;
    UART1_BRR1 = CALC_BRR1(19200);
    UART1_BRR2 = CALC_BRR2(19200);
}

void uartTxData(uint8_t data)
{
    uint32_t tOut = 10e8;
    while( ((UART1_SR & TXE) == 0) && ((--tOut) > 0) );
    UART1_DR = data;
}

void uartSetBaud(uint16_t baud)
{
    UART1_BRR1 = CALC_BRR1(baud);
    UART1_BRR2 = CALC_BRR2(baud);
}

uint8_t getRx()
{
    return UART1_DR;
}
