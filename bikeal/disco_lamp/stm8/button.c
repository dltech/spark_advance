/*
 * STM8 basic support library. STM8 interrupt based button callbacks.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "system.h"
#include "gpio.h"
#include "button.h"

void butonInit(uint8_t port, uint8_t pin, uint8_t priority)
{
    portConfig(port, pin, INPUT_PULLUP_EXTI);
    switch(port)
    {
        case GPIOA:
            EXTI_CR1 &= ~PAIS_MASK;
            EXTI_CR1 = PAIS_FALL_LOW;
            setPriority(EXTI0_ITN, priority);
            return;
        case GPIOB:
            EXTI_CR1 &= ~PBIS_MASK;
            EXTI_CR1 = PBIS_FALL_LOW;
            setPriority(EXTI1_ITN, priority);
            return;
        case GPIOC:
            EXTI_CR1 &= ~PCIS_MASK;
            EXTI_CR1 = PCIS_FALL_LOW;
            setPriority(EXTI2_ITN, priority);
            return;
        case GPIOD:
            EXTI_CR1 &= ~PDIS_MASK;
            EXTI_CR1 = PDIS_FALL_LOW;
            setPriority(EXTI3_ITN, priority);
            return;
        case GPIOE:
            EXTI_CR2 &= ~PEIS_MASK;
            EXTI_CR2 = PEIS_FALL_LOW;
            setPriority(EXTI4_ITN, priority);
            return;
    }
}


