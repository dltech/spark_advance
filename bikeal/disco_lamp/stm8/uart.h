#ifndef H_UART
#define H_UART
/*
 * STM8 basic support library. UART standard functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "regs/uart_reg.h"
#include "gpio.h"

#define UART_PORT   GPIOD
#define UART_TX_PIN GPIO5
#define UART_RX_PIN GPIO6

void uartMinimalInit(void);
void uartTxData(uint8_t data);
void uartSetBaud(uint16_t baud);
uint8_t getRx(void);

#endif
