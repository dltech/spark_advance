#ifndef H_RGB_UART
#define H_RGB_UART
/*
 * Disco ligts. Wire control of RGB lamp through UART.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "stm8/uart.h"

#define START_BYTE  0xaa
#define STOP_BYTE   0xee
#define PACK_SIZE   14

void rgbUartInit(void);
void rgbPackHandler(uint8_t data);
void rgbSetPack(uint8_t *pack);

#endif
