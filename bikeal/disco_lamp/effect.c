/*
 * Disco ligts. 12 channel program kHz PWM.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "stm8/regs/tim_reg.h"
#include "stm8/system.h"
#include "stm8/random127.h"
#include "stm8/button.h"
#include "stm8/gpio.h"
#include "pwm.h"
#include "effect.h"

extern uint8_t  pwmPack[12];

static uint8_t aimColours[4];
static uint8_t effectCnt = EFFECTS_NUM;

void iridescent(uint8_t led, uint8_t *aimColour);
void nextColour(uint8_t led, uint8_t *colour);

void effectInit()
{
    TIM1_PSCRH = 0;
    TIM1_PSCRL = 8;
    TIM1_ARRH = 255;
    TIM1_ARRL = 255;
    TIM1_CR1 =  CEN;
    TIM1_EGR =  UG;
    TIM1_SR1 = 0;
    TIM1_SR2 = 0;
    TIM1_IER |= UIE;
    setPriority(TIM1_UPD_ITN, LEVEL2);

    butonInit(BUTTON_PORT, BUTTON_PIN, LEVEL3);
}

void setEffect(uint8_t n)
{
    switch(n)
    {
        case 0:
            rainbowInit();
            break;
        case 1:
            rainbowDiscreteInit();
            break;
        case 2:
            swapColourInit();
            break;
        case 3:
            fireInit();
            break;
        case 4:
            for(uint8_t i=0 ; i<12 ; ++i) {
                setDuty(i, 0);
            }
            break;
        default:
            effectCnt = 0;
    }
}

void effectSwitch()
{
    delay(20);
    if(readPin(BUTTON_PORT, BUTTON_PIN)) {
        return;
    }
    ++effectCnt;
    if(effectCnt > EFFECTS_NUM) {
        effectCnt = 0;
    }
    setEffect(effectCnt);
}

void effectProcess()
{
    switch(effectCnt)
    {
        case 0:
            rainbow();
            break;
        case 1:
            rainbowDiscrete();
            break;
        case 2:
            swapColour();
            break;
        case 3:
            fire();
            break;
        case 4:
            break;
        default:
            effectCnt = 0;
    }
}

void iridescent(uint8_t led, uint8_t *aimColour)
{
    switch(*aimColour)
    {
        case BLACK_TO_BLACK:
            setDuty(led,0);
            setDuty(led+1,0);
            setDuty(led+2,0);
            *aimColour = BLACK_TO_RED;
            break;
        case BLACK_TO_RED:
            if( pwmPack[led] < 255 ) {
                setDuty(led, pwmPack[led]+RAINBOW_STEP);
            } else {
                *aimColour = RED_TO_MAGENTA;
            }
            break;
        case RED_TO_MAGENTA:
            if( pwmPack[led+B] < 255 ) {
                setDuty(led+B, pwmPack[led+B]+RAINBOW_STEP);
            } else {
                *aimColour = MAGENTA_TO_BLUE;
            }
            break;
        case MAGENTA_TO_BLUE:
            if( pwmPack[led] > 0 ) {
                setDuty(led, pwmPack[led]-RAINBOW_STEP);
            } else {
                *aimColour = BLUE_TO_CYAN;
            }
            break;
        case BLUE_TO_CYAN:
            if( pwmPack[led+G] < 255 ) {
                setDuty(led+G, pwmPack[led+G]+RAINBOW_STEP);
            } else {
                *aimColour = CYAN_TO_GREEN;
            }
            break;
        case CYAN_TO_GREEN:
            if( pwmPack[led+B] > 0 ) {
                setDuty(led+B, pwmPack[led+B]-RAINBOW_STEP);
            } else {
                *aimColour = GREEN_TO_YELLOW;
            }
            break;
        case GREEN_TO_YELLOW:
            if( pwmPack[led] < 255 ) {
                setDuty(led, pwmPack[led]+RAINBOW_STEP);
            } else {
                *aimColour = YELLOW_TO_RED;
            }
            break;
        case YELLOW_TO_RED:
            if( pwmPack[led+G] > 0 ) {
                setDuty(led+G, pwmPack[led+G]-RAINBOW_STEP);
            } else {
                *aimColour = RED_TO_MAGENTA;
            }
            break;
        default:
            *aimColour = BLACK_TO_BLACK;
    }
}

void nextColour(uint8_t led, uint8_t *colour)
{
    switch(*colour)
    {
        case BLACK:
            setDuty(led,0);
            setDuty(led+1,0);
            setDuty(led+2,0);
            *colour = RED;
            break;
        case RED:
            setDuty(led  ,255);
            setDuty(led+1,0);
            setDuty(led+2,0);
            *colour = MAGENTA;
            break;
        case MAGENTA:
            setDuty(led  ,255);
            setDuty(led+1,0);
            setDuty(led+2,255);
            *colour = BLUE;
            break;
        case BLUE:
            setDuty(led  ,0);
            setDuty(led+1,0);
            setDuty(led+2,255);
            *colour = CYAN;
            break;
        case CYAN:
            setDuty(led  ,0);
            setDuty(led+1,255);
            setDuty(led+2,255);
            *colour = GREEN;
            break;
        case GREEN:
            setDuty(led  ,0);
            setDuty(led+1,255);
            setDuty(led+2,0);
            *colour = YELLOW;
            break;
        case YELLOW:
            setDuty(led  ,255);
            setDuty(led+1,255);
            setDuty(led+2,0);
            *colour = RED;
            break;
        default:
            *colour = BLACK;
    }
}

void rainbowInit()
{
    aimColours[0] =  RED_TO_MAGENTA;
    setDuty(R1, 255);
    setDuty(G1, 0);
    setDuty(B1, 0);
    aimColours[1] =  MAGENTA_TO_BLUE;
    setDuty(R2, 120);
    setDuty(G2, 0);
    setDuty(B2, 255);
    aimColours[2] =  CYAN_TO_GREEN;
    setDuty(R3, 0);
    setDuty(G3, 255);
    setDuty(B3, 255);
    aimColours[3] =  GREEN_TO_YELLOW;
    setDuty(R4, 135);
    setDuty(G4, 255);
    setDuty(B4, 0);
}

void rainbow()
{
    for(uint8_t i=0 ; i<4 ; ++i) {
        iridescent(i*3, &(aimColours[i]));
    }
}

void rainbowDiscreteInit()
{
    aimColours[0] =  RED;
    aimColours[1] =  BLUE;
    aimColours[2] =  GREEN;
    aimColours[3] =  YELLOW;
}

void rainbowDiscrete()
{
    static uint8_t cnt=0;
    if(cnt < 30){
        ++cnt;
        return;
    }
    cnt = 0;

    for(uint8_t i=0 ; i<4 ; ++i) {
        nextColour(i*3, &(aimColours[i]));
    }
}

void swapColourInit()
{
    for(uint8_t i=0 ; i<12 ; ++i) {
        if(i%3) {
            setDuty(i,0);
        } else {
            setDuty(i,255);
        }
    }
    for(uint8_t i=0 ; i<4 ; ++i) {
        aimColours[i] = RED;
    }
}

void swapColour()
{
    static uint8_t cnt=0;
    if(cnt < 50){
        ++cnt;
        return;
    }
    cnt = 0;

    nextColour(0, &(aimColours[0]));
    for(uint8_t i=3 ; i<12 ; ++i) {
        setDuty(i,pwmPack[i%3]);
    }
}

void fireInit()
{
    srand127(TIM2_CNTRL/2);
}

void fire()
{
    uint8_t red;
    for(int i=0 ; i<12 ; i+=3)
    {
        red = rand127()*2;

        if( red > 230 ) {
            setDuty(i,   255);
            setDuty(i+G, 255);
            setDuty(i+B, 255);
        } else {
            setDuty(i, red);
            setDuty(i+G, (rand127()/2));
            setDuty(i+B, 0);
        }
    }
}
