;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.2.0 #13081 (Linux)
;--------------------------------------------------------
	.module main
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _buttonPush
	.globl _effects
	.globl _uart_rx
	.globl _main
	.globl _effectProcess
	.globl _effectSwitch
	.globl _effectInit
	.globl _rgbPackHandler
	.globl _rgbUartInit
	.globl _pwmCycle
	.globl _pwmInit
	.globl _enableInterrupt
	.globl _disableInterrupt
	.globl _clockTo16Hsi
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; Stack segment in internal ram
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; interrupt vector
;--------------------------------------------------------
	.area HOME
__interrupt_vect:
	int s_GSINIT ; reset
	int 0x000000 ; trap
	int 0x000000 ; int0
	int 0x000000 ; int1
	int 0x000000 ; int2
	int _buttonPush ; int3
	int 0x000000 ; int4
	int 0x000000 ; int5
	int 0x000000 ; int6
	int 0x000000 ; int7
	int 0x000000 ; int8
	int 0x000000 ; int9
	int 0x000000 ; int10
	int _effects ; int11
	int 0x000000 ; int12
	int 0x000000 ; int13
	int 0x000000 ; int14
	int 0x000000 ; int15
	int 0x000000 ; int16
	int 0x000000 ; int17
	int _uart_rx ; int18
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
__sdcc_init_data:
; stm8_genXINIT() start
	ldw x, #l_DATA
	jreq	00002$
00001$:
	clr (s_DATA - 1, x)
	decw x
	jrne	00001$
00002$:
	ldw	x, #l_INITIALIZER
	jreq	00004$
00003$:
	ld	a, (s_INITIALIZER - 1, x)
	ld	(s_INITIALIZED - 1, x), a
	decw	x
	jrne	00003$
00004$:
; stm8_genXINIT() end
	.area GSFINAL
	jp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
__sdcc_program_startup:
	jp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
;	main.c: 7: int main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c: 9: clockTo16Hsi();
	call	_clockTo16Hsi
;	main.c: 10: pwmInit();
	call	_pwmInit
;	main.c: 11: rgbUartInit();
	call	_rgbUartInit
;	main.c: 12: effectInit();
	call	_effectInit
;	main.c: 14: while(1) {
00102$:
;	main.c: 15: pwmCycle();
	call	_pwmCycle
	jra	00102$
;	main.c: 17: }
	ret
;	main.c: 19: void uart_rx(void) __interrupt(UART1_RX_ITN)
;	-----------------------------------------
;	 function uart_rx
;	-----------------------------------------
_uart_rx:
	div	x, a
	push	a
;	main.c: 21: uint8_t irq = disableInterrupt();
	call	_disableInterrupt
	ld	(0x01, sp), a
;	main.c: 22: uint8_t data = UART1_DR;
	ld	a, 0x5231
;	main.c: 23: rgbPackHandler(data);
	call	_rgbPackHandler
;	main.c: 24: UART1_SR = 0;
	mov	0x5230+0, #0x00
;	main.c: 25: enableInterrupt(irq);
	ld	a, (0x01, sp)
	call	_enableInterrupt
;	main.c: 26: }
	pop	a
	iret
;	main.c: 28: void effects(void) __interrupt(TIM1_UPD_ITN)
;	-----------------------------------------
;	 function effects
;	-----------------------------------------
_effects:
	div	x, a
;	main.c: 30: uint8_t irq = disableInterrupt();
	call	_disableInterrupt
;	main.c: 31: effectProcess();
	push	a
	call	_effectProcess
	pop	a
;	main.c: 32: TIM1_SR1 = 0;
	mov	0x5255+0, #0x00
;	main.c: 33: enableInterrupt(irq);
	call	_enableInterrupt
;	main.c: 34: }
	iret
;	main.c: 36: void buttonPush(void) __interrupt(EXTI0_ITN)
;	-----------------------------------------
;	 function buttonPush
;	-----------------------------------------
_buttonPush:
	div	x, a
;	main.c: 38: uint8_t irq = disableInterrupt();
	call	_disableInterrupt
;	main.c: 39: effectSwitch();
	push	a
	call	_effectSwitch
	pop	a
;	main.c: 40: enableInterrupt(irq);
	call	_enableInterrupt
;	main.c: 41: }
	iret
	.area CODE
	.area CONST
	.area INITIALIZER
	.area CABS (ABS)
