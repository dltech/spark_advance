                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.2.0 #13081 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _buttonPush
                                     12 	.globl _effects
                                     13 	.globl _uart_rx
                                     14 	.globl _main
                                     15 	.globl _effectProcess
                                     16 	.globl _effectSwitch
                                     17 	.globl _effectInit
                                     18 	.globl _rgbPackHandler
                                     19 	.globl _rgbUartInit
                                     20 	.globl _pwmCycle
                                     21 	.globl _pwmInit
                                     22 	.globl _enableInterrupt
                                     23 	.globl _disableInterrupt
                                     24 	.globl _clockTo16Hsi
                                     25 ;--------------------------------------------------------
                                     26 ; ram data
                                     27 ;--------------------------------------------------------
                                     28 	.area DATA
                                     29 ;--------------------------------------------------------
                                     30 ; ram data
                                     31 ;--------------------------------------------------------
                                     32 	.area INITIALIZED
                                     33 ;--------------------------------------------------------
                                     34 ; Stack segment in internal ram
                                     35 ;--------------------------------------------------------
                                     36 	.area	SSEG
      000248                         37 __start__stack:
      000248                         38 	.ds	1
                                     39 
                                     40 ;--------------------------------------------------------
                                     41 ; absolute external ram data
                                     42 ;--------------------------------------------------------
                                     43 	.area DABS (ABS)
                                     44 
                                     45 ; default segment ordering for linker
                                     46 	.area HOME
                                     47 	.area GSINIT
                                     48 	.area GSFINAL
                                     49 	.area CONST
                                     50 	.area INITIALIZER
                                     51 	.area CODE
                                     52 
                                     53 ;--------------------------------------------------------
                                     54 ; interrupt vector
                                     55 ;--------------------------------------------------------
                                     56 	.area HOME
      008000                         57 __interrupt_vect:
      008000 82 00 80 57             58 	int s_GSINIT ; reset
      008004 82 00 00 00             59 	int 0x000000 ; trap
      008008 82 00 00 00             60 	int 0x000000 ; int0
      00800C 82 00 00 00             61 	int 0x000000 ; int1
      008010 82 00 00 00             62 	int 0x000000 ; int2
      008014 82 00 80 BD             63 	int _buttonPush ; int3
      008018 82 00 00 00             64 	int 0x000000 ; int4
      00801C 82 00 00 00             65 	int 0x000000 ; int5
      008020 82 00 00 00             66 	int 0x000000 ; int6
      008024 82 00 00 00             67 	int 0x000000 ; int7
      008028 82 00 00 00             68 	int 0x000000 ; int8
      00802C 82 00 00 00             69 	int 0x000000 ; int9
      008030 82 00 00 00             70 	int 0x000000 ; int10
      008034 82 00 80 AC             71 	int _effects ; int11
      008038 82 00 00 00             72 	int 0x000000 ; int12
      00803C 82 00 00 00             73 	int 0x000000 ; int13
      008040 82 00 00 00             74 	int 0x000000 ; int14
      008044 82 00 00 00             75 	int 0x000000 ; int15
      008048 82 00 00 00             76 	int 0x000000 ; int16
      00804C 82 00 00 00             77 	int 0x000000 ; int17
      008050 82 00 80 94             78 	int _uart_rx ; int18
                                     79 ;--------------------------------------------------------
                                     80 ; global & static initialisations
                                     81 ;--------------------------------------------------------
                                     82 	.area HOME
                                     83 	.area GSINIT
                                     84 	.area GSFINAL
                                     85 	.area GSINIT
      008057                         86 __sdcc_init_data:
                                     87 ; stm8_genXINIT() start
      008057 AE 02 45         [ 2]   88 	ldw x, #l_DATA
      00805A 27 07            [ 1]   89 	jreq	00002$
      00805C                         90 00001$:
      00805C 72 4F 00 00      [ 1]   91 	clr (s_DATA - 1, x)
      008060 5A               [ 2]   92 	decw x
      008061 26 F9            [ 1]   93 	jrne	00001$
      008063                         94 00002$:
      008063 AE 00 02         [ 2]   95 	ldw	x, #l_INITIALIZER
      008066 27 09            [ 1]   96 	jreq	00004$
      008068                         97 00003$:
      008068 D6 80 7F         [ 1]   98 	ld	a, (s_INITIALIZER - 1, x)
      00806B D7 02 45         [ 1]   99 	ld	(s_INITIALIZED - 1, x), a
      00806E 5A               [ 2]  100 	decw	x
      00806F 26 F7            [ 1]  101 	jrne	00003$
      008071                        102 00004$:
                                    103 ; stm8_genXINIT() end
                                    104 	.area GSFINAL
      00807D CC 80 54         [ 2]  105 	jp	__sdcc_program_startup
                                    106 ;--------------------------------------------------------
                                    107 ; Home
                                    108 ;--------------------------------------------------------
                                    109 	.area HOME
                                    110 	.area HOME
      008054                        111 __sdcc_program_startup:
      008054 CC 80 82         [ 2]  112 	jp	_main
                                    113 ;	return from main will return to caller
                                    114 ;--------------------------------------------------------
                                    115 ; code
                                    116 ;--------------------------------------------------------
                                    117 	.area CODE
                                    118 ;	main.c: 7: int main(void)
                                    119 ;	-----------------------------------------
                                    120 ;	 function main
                                    121 ;	-----------------------------------------
      008082                        122 _main:
                                    123 ;	main.c: 9: clockTo16Hsi();
      008082 CD 83 25         [ 4]  124 	call	_clockTo16Hsi
                                    125 ;	main.c: 10: pwmInit();
      008085 CD 85 CD         [ 4]  126 	call	_pwmInit
                                    127 ;	main.c: 11: rgbUartInit();
      008088 CD 85 58         [ 4]  128 	call	_rgbUartInit
                                    129 ;	main.c: 12: effectInit();
      00808B CD 88 5A         [ 4]  130 	call	_effectInit
                                    131 ;	main.c: 14: while(1) {
      00808E                        132 00102$:
                                    133 ;	main.c: 15: pwmCycle();
      00808E CD 88 2C         [ 4]  134 	call	_pwmCycle
      008091 20 FB            [ 2]  135 	jra	00102$
                                    136 ;	main.c: 17: }
      008093 81               [ 4]  137 	ret
                                    138 ;	main.c: 19: void uart_rx(void) __interrupt(UART1_RX_ITN)
                                    139 ;	-----------------------------------------
                                    140 ;	 function uart_rx
                                    141 ;	-----------------------------------------
      008094                        142 _uart_rx:
      008094 62               [ 2]  143 	div	x, a
      008095 88               [ 1]  144 	push	a
                                    145 ;	main.c: 21: uint8_t irq = disableInterrupt();
      008096 CD 83 CE         [ 4]  146 	call	_disableInterrupt
      008099 6B 01            [ 1]  147 	ld	(0x01, sp), a
                                    148 ;	main.c: 22: uint8_t data = UART1_DR;
      00809B C6 52 31         [ 1]  149 	ld	a, 0x5231
                                    150 ;	main.c: 23: rgbPackHandler(data);
      00809E CD 85 69         [ 4]  151 	call	_rgbPackHandler
                                    152 ;	main.c: 24: UART1_SR = 0;
      0080A1 35 00 52 30      [ 1]  153 	mov	0x5230+0, #0x00
                                    154 ;	main.c: 25: enableInterrupt(irq);
      0080A5 7B 01            [ 1]  155 	ld	a, (0x01, sp)
      0080A7 CD 83 DE         [ 4]  156 	call	_enableInterrupt
                                    157 ;	main.c: 26: }
      0080AA 84               [ 1]  158 	pop	a
      0080AB 80               [11]  159 	iret
                                    160 ;	main.c: 28: void effects(void) __interrupt(TIM1_UPD_ITN)
                                    161 ;	-----------------------------------------
                                    162 ;	 function effects
                                    163 ;	-----------------------------------------
      0080AC                        164 _effects:
      0080AC 62               [ 2]  165 	div	x, a
                                    166 ;	main.c: 30: uint8_t irq = disableInterrupt();
      0080AD CD 83 CE         [ 4]  167 	call	_disableInterrupt
                                    168 ;	main.c: 31: effectProcess();
      0080B0 88               [ 1]  169 	push	a
      0080B1 CD 88 E8         [ 4]  170 	call	_effectProcess
      0080B4 84               [ 1]  171 	pop	a
                                    172 ;	main.c: 32: TIM1_SR1 = 0;
      0080B5 35 00 52 55      [ 1]  173 	mov	0x5255+0, #0x00
                                    174 ;	main.c: 33: enableInterrupt(irq);
      0080B9 CD 83 DE         [ 4]  175 	call	_enableInterrupt
                                    176 ;	main.c: 34: }
      0080BC 80               [11]  177 	iret
                                    178 ;	main.c: 36: void buttonPush(void) __interrupt(EXTI0_ITN)
                                    179 ;	-----------------------------------------
                                    180 ;	 function buttonPush
                                    181 ;	-----------------------------------------
      0080BD                        182 _buttonPush:
      0080BD 62               [ 2]  183 	div	x, a
                                    184 ;	main.c: 38: uint8_t irq = disableInterrupt();
      0080BE CD 83 CE         [ 4]  185 	call	_disableInterrupt
                                    186 ;	main.c: 39: effectSwitch();
      0080C1 88               [ 1]  187 	push	a
      0080C2 CD 88 C4         [ 4]  188 	call	_effectSwitch
      0080C5 84               [ 1]  189 	pop	a
                                    190 ;	main.c: 40: enableInterrupt(irq);
      0080C6 CD 83 DE         [ 4]  191 	call	_enableInterrupt
                                    192 ;	main.c: 41: }
      0080C9 80               [11]  193 	iret
                                    194 	.area CODE
                                    195 	.area CONST
                                    196 	.area INITIALIZER
                                    197 	.area CABS (ABS)
