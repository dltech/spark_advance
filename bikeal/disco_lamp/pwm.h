#ifndef H_PWM
#define H_PWM
/*
 * Disco ligts. 12 channel program kHz PWM.
 *
 * Copyright 2022 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "stm8/gpio.h"

#define PORT_R1 GPIOB
#define GPIO_R1 GPIO4
#define PORT_G1 GPIOB
#define GPIO_G1 GPIO5
#define PORT_B1 GPIOA
#define GPIO_B1 GPIO3

#define PORT_R2 GPIOC
#define GPIO_R2 GPIO5
#define PORT_G2 GPIOC
#define GPIO_G2 GPIO4
#define PORT_B2 GPIOC
#define GPIO_B2 GPIO3

#define PORT_R3 GPIOD
#define GPIO_R3 GPIO2
#define PORT_G3 GPIOC
#define GPIO_G3 GPIO7
#define PORT_B3 GPIOC
#define GPIO_B3 GPIO6

#define PORT_R4 GPIOA
#define GPIO_R4 GPIO1
#define PORT_G4 GPIOD
#define GPIO_G4 GPIO4
#define PORT_B4 GPIOD
#define GPIO_B4 GPIO3

#define GPIOA_LED_PINS  (GPIO_B1 | GPIO_R4)
#define GPIOB_LED_PINS  (GPIO_R1 | GPIO_G1)
#define GPIOC_LED_PINS  (GPIO_R2 | GPIO_G2 | GPIO_B2 | GPIO_G3 | GPIO_B3)
#define GPIOD_LED_PINS  (GPIO_R3 | GPIO_G4 | GPIO_B4)

#define R   0
#define G   1
#define B   2
#define R1  R
#define G1  G
#define B1  B
#define LED1    R1
#define R2  3
#define G2  4
#define B2  5
#define LED2    R2
#define R3  6
#define G3  7
#define B3  8
#define LED3    R3
#define R4  9
#define G4  10
#define B4  11
#define LED4    R4

void pwmInit(void);
void setDuty(uint8_t ch, uint8_t duty);
void pwmCycle(void);

#endif
