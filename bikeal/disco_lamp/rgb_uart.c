/*
 * Disco ligts. Wire control of RGB lamp through UART.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "stm8/uart.h"
#include "stm8/system.h"
#include "pwm.h"
#include "effect.h"
#include "rgb_uart.h"

void rgbUartInit()
{
    uartMinimalInit();
    uartSetBaud(19200);
    setPriority(UART1_RX_ITN, LEVEL2);
}

void rgbPackHandler(uint8_t data)
{
    static uint8_t pack[PACK_SIZE];
    for(int i=0 ; i<(PACK_SIZE-1) ; ++i) {
        pack[i] = pack[i+1];
    }
    pack[PACK_SIZE-1] = data;

    if((pack[0] == START_BYTE) && (pack[PACK_SIZE-1] == STOP_BYTE)) {
        setEffect(EFFECTS_NUM);
        rgbSetPack(pack + 1);
    }
}

void rgbSetPack(uint8_t *pack)
{
    for(uint8_t i=0 ; i<12 ; i++) {
        setDuty(i, pack[i]);
    }
}
