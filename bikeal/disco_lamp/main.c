#include "stm8/system.h"
#include "stm8/regs/tim_reg.h"
#include "pwm.h"
#include "rgb_uart.h"
#include "effect.h"

int main(void)
{
    clockTo16Hsi();
    pwmInit();
    rgbUartInit();
    effectInit();

    while(1) {
        pwmCycle();
    }
}

void uart_rx(void) __interrupt(UART1_RX_ITN)
{
    uint8_t irq = disableInterrupt();
    uint8_t data = UART1_DR;
    rgbPackHandler(data);
    UART1_SR = 0;
    enableInterrupt(irq);
}

void effects(void) __interrupt(TIM1_UPD_ITN)
{
    uint8_t irq = disableInterrupt();
    effectProcess();
    TIM1_SR1 = 0;
    enableInterrupt(irq);
}

void buttonPush(void) __interrupt(EXTI0_ITN)
{
    uint8_t irq = disableInterrupt();
    effectSwitch();
    enableInterrupt(irq);
}
