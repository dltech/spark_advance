#ifndef H_EFFECT
#define H_EFFECT
/*
 * Disco ligts. 12 channel program kHz PWM.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define RAINBOW_STEP    15
#define EFFECTS_NUM     4

enum{
    BLACK_TO_BLACK = 0,
    BLACK_TO_RED,
    RED_TO_MAGENTA,
    MAGENTA_TO_BLUE,
    BLUE_TO_CYAN,
    CYAN_TO_GREEN,
    GREEN_TO_YELLOW,
    YELLOW_TO_RED
};

enum{
    BLACK = 0,
    RED,
    MAGENTA,
    BLUE,
    CYAN,
    GREEN,
    YELLOW,
};

#define BUTTON_PORT GPIOA
#define BUTTON_PIN  GPIO2

void effectInit(void);
void setEffect(uint8_t n);
void effectSwitch(void);
void effectProcess(void);
void rainbowInit(void);
void rainbowDiscreteInit(void);
void swapColourInit(void);
void fireInit(void);

void rainbow(void);
void rainbowDiscrete(void);
void swapColour(void);
void fire(void);


#endif
