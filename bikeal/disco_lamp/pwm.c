/*
 * Disco ligts. 12 channel program kHz PWM.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "stm8/regs/tim_reg.h"
#include "gpio.h"
#include "eeprom.h"
#include "pwm.h"

static uint8_t gpioab_pwm[256];
static uint8_t gpioc_pwm[256];
//volatile uint8_t gpiod_pwm[256];
volatile uint8_t  pwmPack[12];
static uint8_t *pwmPorts[12];
static uint8_t  pwmPins[12];

void pwmInit()
{
    // init pwm gpio
    portConfig(GPIOA, GPIO1 | GPIO3, OUTPUT_PP_10M);
    portConfig(GPIOB, GPIO4 | GPIO5, OUTPUT_OD_10M);
    portConfig(GPIOC, GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7, OUTPUT_PP_10M);
    portConfig(GPIOD, GPIO2 | GPIO3 | GPIO4, OUTPUT_PP_10M);
    // init pwm arrays
    for(uint8_t i=0 ; i<255 ; i++) {
        gpioab_pwm[i] = 0;
        gpioc_pwm[i] = 0;
//        gpiod_pwm[i] = 0;
    }
    gpioab_pwm[255] = 0;
    gpioc_pwm[255] = 0;
//    gpiod_pwm[255] = 0;
    for(uint8_t i=0 ; i<12 ; ++i) {
        pwmPack[i] = 0;
    }
    pwmPorts[0]  = gpioab_pwm;
    pwmPins[0]   = GPIO_R1;
    pwmPorts[1]  = gpioab_pwm;
    pwmPins[1]   = GPIO_G1;
    pwmPorts[2]  = gpioab_pwm;
    pwmPins[2]   = GPIO_B1;
    pwmPorts[3]  = gpioc_pwm;
    pwmPins[3]   = GPIO_R2;
    pwmPorts[4]  = gpioc_pwm;
    pwmPins[4]   = GPIO_G2;
    pwmPorts[5]  = gpioc_pwm;
    pwmPins[5]   = GPIO_B2;
    pwmPorts[6]  = gpioab_pwm;//gpiod_pwm;
    pwmPins[6]   = GPIO_R3;
    pwmPorts[7]  = gpioc_pwm;
    pwmPins[7]   = GPIO_G3;
    pwmPorts[8]  = gpioc_pwm;
    pwmPins[8]   = GPIO_B3;
    pwmPorts[9]  = gpioab_pwm;
    pwmPins[9]   = GPIO_R4;
    pwmPorts[10] = gpioab_pwm;//gpiod_pwm;
    pwmPins[10]  = GPIO_G4;
    pwmPorts[11] = gpioab_pwm;//gpiod_pwm;
    pwmPins[11]  = GPIO_B4;

    if(OPT2 != AFR1) {
        unlockOpt();
        writeOpt(OPT2_ADDR, AFR1);
        lockOpt();
    }

    // init timer
    TIM2_PSCR  = 5;
    TIM2_ARRH  = 0;
    TIM2_ARRL  = 255;
    TIM2_CCMR1 = T2_OC1M_PWM1 | T2_CC1S_OUT;
    TIM2_CCMR2 = T2_OC2M_PWM1 | T2_CC2S_OUT;
    TIM2_CCMR3 = T2_OC3M_PWM1 | T2_CC3S_OUT;
    TIM2_CCER1 = T2_CC2E | T2_CC1E;
    TIM2_CCER2 = T2_CC3E;
    TIM2_CCR1H = 0;
    TIM2_CCR2H = 0;
    TIM2_CCR3H = 0;
    TIM2_CCR1L = 0;
    TIM2_CCR2L = 0;
    TIM2_CCR3L = 0;
    TIM2_EGR   = T2_UG;
    TIM2_SR1   = 0;
    TIM2_SR2   = 0;
    TIM2_CR1   = T2_CEN;
}

void setDuty(uint8_t ch, uint8_t duty)
{
    if( ch == R3 ) {
        TIM2_CCR3L = duty;
        pwmPack[ch] = duty;
        return;
    }
    if( ch == G4 ) {
        TIM2_CCR1L = duty;
        pwmPack[ch] = duty;
        return;
    }
    if( ch == B4 ) {
        TIM2_CCR2L = duty;
        pwmPack[ch] = duty;
        return;
    }
    if(duty>=pwmPack[ch]) {
        (pwmPorts[ch])[duty] |= pwmPins[ch];
    }
    if(duty<=pwmPack[ch]) {
        (pwmPorts[ch])[pwmPack[ch]] &= ~pwmPins[ch];
    }
    for(uint8_t i=pwmPack[ch] ; i<=(duty-1) ; i++) {
        (pwmPorts[ch])[i] |= pwmPins[ch];
    }
    for(uint8_t i=duty ; i<=(pwmPack[ch]-1) ; i++) {
        (pwmPorts[ch])[i] &= ~pwmPins[ch];
    }
    pwmPack[ch] = duty;
}

void pwmCycle()
{
    static uint8_t prev=0;
    // for(uint8_t i=0 ; i<12 ; i++) {
    //     if(prev >= pwmPack[i]) {
    //         P_ODR(pwmPortss[i]) |= pwmPins[i];
    //     }
    // }
    // if(TIM4_SR == T4_UIF) {
    //     PA_ODR = 0;
    //     PB_ODR = 0;
    //     PC_ODR = 0;
    //     PD_ODR = 0;
    //     TIM4_SR = 0;
    // }
    uint8_t gpioab = gpioab_pwm[prev];
    PA_ODR = gpioab&GPIOA_LED_PINS;
    PB_ODR = gpioab&GPIOB_LED_PINS;
    PC_ODR = gpioc_pwm[prev];
//    PD_ODR = gpiod_pwm[prev];
    while(TIM2_CNTRL == prev);
    prev = TIM2_CNTRL;
}
