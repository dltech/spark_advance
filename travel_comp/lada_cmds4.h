#ifndef H_LADA_CMDS
#define H_LADA_CMDS
/*
 * Janvary 4 command system. Here are commands of standard ECU protocol.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LADA_BAUD   9600

#define KP_BYTE 0x0d
#define LSB_0D  0x40
#define MSB_0D  0xcd
#define LSB_40  0x40
#define MSB_40  0x00

// Low level commands
#define CVERS       0x01 /* Command monitor version */
#define CREADI      0x11 /* Read RAM byte */
#define CWRITI      0x12 /* Write RAM byte */
#define CREADX      0x13 /* Read XRAM byte */
#define CWRITX      0x14 /* Write XRAM byte */
#define CREADC      0x15 /* Read CODE byte */
#define CWRITC      0x16 /* Write CODE byte */
#define CREADDI     0x21 /* Read an array of RAM bytes */
#define CWRITDI     0x22 /* Write an array of RAM bytes */
#define CREADDX     0x23 /* Read an array of XRAM bytes */
#define CWRITDX     0x24 /* Write an array of XRAM bytes */
#define CREADDC     0x25 /* Read an array of CODE bytes */
#define CWRITDC     0x26 /* Write an array of CODE bytes */
#define CREADSFR    0x31 /* Read SFR */
#define CWRITSFR    0x32 /* Write SFR */
#define CGOTO       0x41 /* Set address */

// High level single comands
#define CPASP1      0x51 /* Read programme passport */
#define CPASP2      0x52 /* Read programme passport */
#define CPASP3      0x53 /* Read programme passport */
#define CPASD1      0x54 /* Read data passport */
#define CPASD2      0x55 /* Read data passport */
#define CPASD3      0x56 /* Read data passport */
#define CPASD4      0x57 /* Read data passport */
#define CPASD5      0x58 /* Read data passport */
#define CNUMPAR     0x60 /* Read a multitude of parameters */
#define CREADP      0x61 /* Single read of a number of parameters */
#define CWRITP      0x62 /* Write parameters */
#define CREADL      0x63 /* Read a list of parameters */
#define CWRITL      0x64 /* Write a list of parameters */

// High level multiple comands
#define CREADPF     0x71 /* Continuous read of a number of parameters, 1st level */
#define CREADPS     0x7f /* Continuous read of a number of parameters, 2nd level */
#define SYNC1       0x80 /* Sync by 1st tact */
#define SYNC2       0x81 /* Sync by 2nd tact */
#define SYNC3       0x82 /* Sync by 3rd tact */
#define SYNC4       0x83 /* Sync by 4th tact */

// glismon.72 module varialnles
#define TRANRAM     1  /* */
#define EXBPORT     2  /* */
#define EXCPORT     3  /* */
#define EXCL        4  /* */
#define EXCM        5  /* */
#define EXCFOR      6  /* */
#define EXCPROG     7  /* */
#define ICERROR     8  /* error flags */
#define SERRORL     9  /* error flags */
#define SERRORH     10 /* error flags */
#define PROLAM      11 /* Previous INLAM (lambda sensor signal) */
#define TYPE        12 /* Configuration (0 - lambda sensor, 1 - adsorber,
                          2 - recirculation valve, 3 - knock sensor,
                          4 - temperature sensor, 5 - phase sensor) */
#define FREQ        13 /* Сrankshaft speed */
#define NFREQ       14 /* Quantized rankshaft speed (16) */
#define NBFREQ      15 /* Quantized rankshaft speed (32) */
#define FREQX       16 /* Idle crancshaft speed */
#define EFREQ       17 /* FREQ error */
#define JUFRXX      18 /* Idle FREQ table setting */
#define JDUFREQ     19 /* Adaptive FREQ table setting shift */
#define JUFREQ      20 /* FREQ setting */
#define JGBCIN      21 /* Output GBC */
#define JGBCD       22 /* Measured GBC */
#define JGBCG       23 /* GBC edge value by throttle valve */
#define JGBC        24 /* Cyclic air flow */
#define NGBC        25 /* Quantized GBC (16) */
#define NBGBC       26 /* Quantized GBC (32) */
#define NFRGBC      27 /* NGBC*16+NFREQ */
#define JTKT256     28 /* Tact counter */
#define JTSYS       29 /* System work time */
#define JTIM256     30 /* Block time counter (increments every 20ms) */
#define JTSTOP      31 /* Time of engine stop after RESET (in seconds) */
#define JAIR        32 /* Air flow */
#define JGTC        33 /* Cyclic fuel injection (1/90 mg per tact) */
#define JQT         34 /* Volumetric fuel consumption */
#define JGTCA       35 /* Asyncronous cyclic supply */
#define TWAT        36 /* Temperature of antifreeze */
#define NTWAT       37 /* Quantized temperature of antifreeze */
#define TAIR        38 /* Air flow */
#define NTAIR       39 /* Quantized air flow */
#define NUACC       40 /* Quantized system voltage */
#define RCO         41 /* CO potentiometer value */
#define THR         42 /* Throttle valve % */
#define JSPEED      43 /* Car speed */
#define JALAM       44 /* Quantized code of oxygen sensor ADC */
#define GB          45 /* Air flow */
#define VALF        46 /* ALF in the interrupt */
#define INJ         47 /* Injection */
#define FAZ         48 /* Injection phase */
#define COEFFF      49 /* Injector output coefficient */
#define KP          50 /* Configuration fuel supply coefficient */
#define UGB         51 /* Bypass air flow */
#define SSM         52 /* Step motor setting */
#define FSM         53 /* Step motor value */
#define JDKGTC      54 /* Dinamical correction GTC coefficient */
#define JKGBC       55 /* GBC coefficient by throttle valve */
#define UOZ         56 /* Ignition angle */
#define UOZOC       57 /* Ignition angle for octane corrector (0,5 deg) */
#define UOZXX       58 /* Idle ignition angle */
#define DUOZ1       59 /* Ignition angle shift by detonation in 1st cylinder */
#define DUOZ2       60 /* Ignition angle shift by detonation in 2nd cylinder */
#define DUOZ3       61 /* Ignition angle shift by detonation in 3rd cylinder */
#define DUOZ4       62 /* Ignition angle shift by detonation in 4th cylinder */
#define JADS        63 /* Adsorber refresh grade */
#define JEGR        64 /* Recycling grade */
#define MODE        65 /* Mode flags */
#define MODE1       66 /* Mode flags */
#define JATHR       67 /* Throttle valve sensor ADC code */
#define JAUACC      68 /* System voltage ADC code */
#define JATWAT      69 /* Antifreeze temperature ADC code */
#define JATAIR      70 /* Air temperature ADC code */
#define JADET       71 /* Knock sensor signal */
#define JARCO       72 /* CO potentiometer signal */
#define JARDIA      73 /* Diagnostics request */
#define TABKF       74
#define TABKF1      75
#define TABKF2      76
#define TABKF3      77
#define TABKF4      78
#define TABKF5      79
#define TABKF6      80
#define TABKF7      81
#define TKFADS      82
#define TKFADS1     83
#define TKFADS2     84
#define TKFADS3     85
#define TKFADS4     86
#define TKFADS5     87
#define TKFADS6     88
#define TKFADS7     89
#define BFKGBC      90
#define COUNTERR    91
// a table of sizes of variables
#define TRANRAM_SIZE    1
#define EXBPORT_SIZE    1
#define EXCPORT_SIZE    1
#define EXCL_SIZE       1
#define EXCM_SIZE       1
#define EXCFOR_SIZE     1
#define EXCPROG_SIZE    1
#define ICERROR_SIZE    1
#define SERRORL_SIZE    1
#define SERRORH_SIZE    1
#define PROLAM_SIZE     1
#define TYPE_SIZE       1
#define FREQ_SIZE       1
#define NFREQ_SIZE      1
#define NBFREQ_SIZE     1
#define FREQX_SIZE      1
#define EFREQ_SIZE      1
#define JUFRXX_SIZE     1
#define JDUFREQ_SIZE    1
#define JUFREQ_SIZE     1
#define JGBCIN_SIZE     2
#define JGBCD_SIZE      2
#define JGBCG_SIZE      2
#define JGBC_SIZE       2
#define NGBC_SIZE       1
#define NBGBC_SIZE      1
#define NFRGBC_SIZE     1
#define JTKT256_SIZE    1
#define JTSYS_SIZE      1
#define JTIM256_SIZE    1
#define JTSTOP_SIZE     1
#define JAIR_SIZE       2
#define JGTC_SIZE       2
#define JQT_SIZE        2
#define JGTCA_SIZE      1
#define TWAT_SIZE       1
#define NTWAT_SIZE      1
#define TAIR_SIZE       1
#define NTAIR_SIZE      1
#define NUACC_SIZE      1
#define RCO_SIZE        1
#define THR_SIZE        1
#define JSPEED_SIZE     1
#define JALAM_SIZE      1
#define GB_SIZE         1
#define VALF_SIZE       1
#define INJ_SIZE        2
#define FAZ_SIZE        1
#define COEFFF_SIZE     1
#define KP_SIZE         1
#define UGB_SIZE        1
#define SSM_SIZE        1
#define FSM_SIZE        1
#define JDKGTC_SIZE     1
#define JKGBC_SIZE      1
#define UOZ_SIZE        1
#define UOZOC_SIZE      1
#define UOZXX_SIZE      1
#define DUOZ1_SIZE      1
#define DUOZ2_SIZE      1
#define DUOZ3_SIZE      1
#define DUOZ4_SIZE      1
#define JADS_SIZE       1
#define JEGR_SIZE       1
#define MODE_SIZE       1
#define MODE1_SIZE      1
#define JATHR_SIZE      1
#define JAUACC_SIZE     1
#define JATWAT_SIZE     1
#define JATAIR_SIZE     1
#define JADET_SIZE      1
#define JARCO_SIZE      1
#define JARDIA_SIZE     1
#define TABKF_SIZE      32
#define TABKF1_SIZE     32
#define TABKF2_SIZE     32
#define TABKF3_SIZE     32
#define TABKF4_SIZE     32
#define TABKF5_SIZE     32
#define TABKF6_SIZE     32
#define TABKF7_SIZE     32
#define TKFADS_SIZE     32
#define TKFADS1_SIZE    32
#define TKFADS2_SIZE    32
#define TKFADS3_SIZE    32
#define TKFADS4_SIZE    1
#define TKFADS5_SIZE    1
#define TKFADS6_SIZE    1
#define TKFADS7_SIZE    1
#define BFKGBC_SIZE     1
#define COUNTERR_SIZE   1


#endif
