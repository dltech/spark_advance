#ifndef H_LADA_CMDS
#define H_LADA_CMDS
/*
 * Список команд, ошибок, тайминги и процие константы протокола
 * ЭБУ Январь 5.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define DEFAULT_BAUD    10500

/* Заголовок k-line */
#define FMT_HEAD_NO_ADDR    0x00
#define FMT_HEAD_CARB       0x40
#define FMT_HEAD_PHYS_ADDR  0x80
#define FMT_HEAD_FUNC_ADDR  0xc0
#define FMT_LEN_MSK         0x3f
#define TGT_ADDR            0x10
#define SRC_ADDR            0xf1
#define MAX_LEN_3BYTE       63
#define MAX_LEN_4BYTE       123

/* Идентификатор запроса Sid */
#define STC     0x81
#define SPC     0x82
#define STDS    0x10
#define SPDS    0x20
#define ER      0x11
#define CDI     0x14
#define RDTCBS  0x18
#define REI     0x1a
#define RDBLI   0x21
#define RMBA    0x23
#define IOCBLI  0x30
#define WDBLI   0x3b
#define TP      0x3e
// Маска положительного ответа
#define SID_SUCCESS_ADD 0x40
// отрицательный ответ
#define SID_ERR         0x7f

/* Функции */
/* Start communication (STC == 0x81) */
#define KB      0x6b6f // KeyBytes типы заголовка и временные параметры обмена.

/* Stop communication (SPC == 0x82) */
// Код ответа
#define GR      0x10 // Запрос откл, но приемник не спец причину откл.
#define SNS     0x11 // приемник не поддерживает данный вид запроса.
#define SFNS_IF 0x12 // не поддерживает аргументы сообщения или формат не соотв.
#define B_RR    0x21 // приемник временно слишком занят
#define ROOR    0x31 // значение выходит за допустимый диапазон.
#define TA      0x72 // процесс был прерван по неизв. причине и не может быть завершен.
#define BTDCE   0x77 // контрольная сумма не соответствует ожидаемой.

/* Start diagnostic session (SPDS == 0x10) */
#define DCM_DTM 0x81 // режим по умолчанию, других нету
// скорость обмена
#define BRM_NBR 0x0a // нормальный, 10400
#define BRM_HBR 0x26 // высокий, 38400
#define BRM_EBR 0x39 // увеличенный, 57600

/* Stop diagnostic session (SPDS == 0x20) */

/* Tester present (TP == 0x3e) */
#define Y   0x01 // должен послать ответное сообщение.
#define NO  0x02 // Не должен послать ответное сообщение.

/* ECU reset (ER == 0x11) */
#define PO  0x01 // Power on, сброс, аналогичный полному аппаратному сбросу

/* Read ECU identification (REI == 0x1a) */
#define ECUIDT  0x80 // ECUIdentificationDataTable получить полную таблицу данных
#define VIN     0x90 // VIN(Vehicle Identification Number)
#define VMECUHN 0x91 // vehicleManufacturerECUHardwareNumber свой заводской номер
#define SSECUHN 0x92 // systemSupplierECUHardwareNumber код блока управления
#define SSECUSN 0x94 // systemSupplierECUSoftwareNumber код ПО блока
#define SNOET   0x97 // systemNameOrEngineType условное наименование системы и тип двигателя.
#define RSC     0x98 // repairShopCode код для запасных частей.
#define PD      0x99 // ProgrammingDate дата подготовки прошивки ПЗУ
#define VMECUID 0x9a // vehicleManufacturerECUIdentifier идентификационные данные производителя.
// Размер параметров из примера (идут без пробелов)
#define VIN_SIZE        19
#define VMECUHN_SIZE    16
#define SSECUHN_SIZE    10
#define SSECUSN_SIZE    10
#define SNOET_SIZE      15
#define RSC_SIZE        7
#define PD_SIZE         10
#define VMECUID_SIZE    8

/* Сlear diagnostic information (CDI == 0x14) */
#define PG  0x0000 // Powertrain Group стирание систем управления двигателем и трансмиссией.
#define AG  0xff00 // All Groups стирание всех систем автомобиля.

/* Read diagnostic trouble codes by status (RDTCBS == 18) */
#define SODTC_RT    0x00 // получить все коды неисправностей вместе с их статусом
#define NODTCS      0x00 // коды неисправностей не обнаружены
#define NRODTCS_MAX 25   // количество кодов ошибок (максимально 25)

/* Read data by local identifier (RDBLI == 0x21) */
// recordLocalIdentifier определяет наборы передаваемых данных
#define RLI_ASS 0x01 // afterSalesServiceRecordLocalIdentifier даннные для СТО
#define RLI_EOL 0x02 // endOfLineRecordLocalIdentifier для конца сборочной линии
#define RLI_FT  0x03 // factoryTestRecordLocalIdentifier при входном контроле
// По этим адресам может быть ничего не записано
#define RLI_IR  0xa0 // immobilizerRecord статус иммобилизатора
#define RLI_BSN 0xa1 // bodySerialNumber серийный номер кузова автомобиля
#define RLI_ESN 0xa2 // engineSerialNumber серийный номер двигателя
#define RLI_MD  0xa3 // manufacturerDate дата изготовления автомобиля
// Максимальные размеры полей
#define RLI_ASS_MAX     128
#define RLI_EOL_MAX     128
#define RLI_FT_MAX      128
#define RLI_IR_SIZE     2
#define RLI_BSN_SIZE    7
#define RLI_ESN_SIZE    7
#define RLI_MD_SIZE     10
/* Для RLI_ASS (0x01) */
#define CONFIGURATION1_POS              3  // Слово комплектации 1
#define CONFIGURATION2_POS              4  // Слово комплектации 2
#define WORK_MODE1_POS                  5  // Слово режима работы 1
#define WORK_MODE2_POS                  6  // Слово режима работы 2
#define CURRENT_FAULT1_POS              7  // Слово флагов текущих неисправностей 1
#define CURRENT_FAULT2_POS              8  // Слово флагов текущих неисправностей 2
#define CURRENT_FAULT3_POS              9  // Слово флагов текущих неисправностей 3
#define CURRENT_FAULT4_POS              10 // Слово флагов текущих неисправностей 4
#define FREESE_TEMP_POS                 11 // N=E-40 [°C]
#define AIR_FUEL_RATIO_POS              12 // N=14.7*(E+128)/256
#define THROTTLE_POS_POS                13 // N=E [%]
#define ENGINE_SPEED_POS                14 // N=E*40 [об/мин]
#define IDLE_ENGINE_SPEED_POS           15 // N=E*10 [об/мин]
#define SET_STEP_POS                    16 // N=E [шагов]
#define CURRENT_STEP_POS                17 // N=E [шагов]
#define INJECTOR_TIME_CORR_POS          18 // N=(E+128)/256
#define SPARK_ADVANCE_POS               19 // N=E/2 [гр.КВ], где E-знаковое
#define CAR_SPEED_POS                   20 // N=E [км/час]
#define SYSTEM_VOLTAGE_POS              21 // N=5.2 + E*0.05 [В]
#define SET_IDLE_ENGINE_SPEED_POS       22 // N=E*10 [об/мин]
#define OXYGEN_SENSOR_VOLTAGE_POS       23 // N=1.25*(Е/256) [В]
#define OXYGEN_SENSOR_FLAGS_POS         24 // Флаги состояния датчика кислорода
#define INJECTION_TIME_LSB_POS          25 // Длительность импульса впрыска
#define INJECTION_TIME_MSB_POS          26 // N=E/125 [мсек]
#define AIR_FLOW_LSB_POS                27 // Массовый расход воздуха (младший байт)
#define AIR_FLOW_MSB_POS                28 // N=E/10 [кг/час]
#define CYCLE_AIR_FLOW_LSB_POS          29 // Цикловой расход воздуха (младший байт)
#define CYCLE_AIR_FLOW_MSB_POS          30 // N=E/6 [мг/такт]
#define HOUR_FUEL_CONSUMPTION_LSB_POS   31 // Часовой расход топлива (младший байт)
#define HOUR_FUEL_CONSUMPTION_MSB_POS   32 // N=E/50[л/час]
#define TRAVEL_FUEL_CONSUMPTION_LSB_POS 33 // Путевой расход топлива(младший байт)
#define TRAVEL_FUEL_CONSUMPTION_MSB_POS 34 // N=E/128[л/100км]
#define CRC_ROM_LSB_POS                 35 // Контрольная сумма ПЗУ
#define CRC_ROM_MSB_POS                 36 // Контрольная сумма ПЗУ
// Описание слова комплектации 1.
#define OXYGEN_SENSOR_FLAG          0x01 // флаг комплектации датчиком кислорода
#define ADSORBER_FLAG               0x02 // флаг комплектации адсорбером
#define RECIRCULATION_VALVE_FLAG    0x04 // флаг комплектации клапаном рециркуляции
#define CNOCK_SENSOR_FLAG           0x08 // флаг комплектации датчиком детонации
#define AIR_TEMP_SENSOR_FLAG        0x10 // флаг комплектации датчиком температуры воздуха
#define PHASE_SENSOR_FLAG           0x20 // флаг комплектации датчиком фазы
#define FUEL_CUT_OFF_BAN_FLAG       0x40 // флаг запрещения отсечки топлива
#define IDLE_ADAPTATION_FLAG        0x80 // флаг разрешения адаптации уставки ХХ
// Описание слова комплектации 2.
#define CO_POTENTIOMETER_FLAG       0x01 // флаг комплектации потенциометром корректировки CO
#define THROTTLE_ADAPTATION_FLAG    0x02 // флаг разрешения адаптации нуля дросселя
#define ASYNC_FUEL_INJ_START_FLAG   0x04 // флаг разрешения асинхронной подачи топлива при пуске
#define SAVE_ERRORS_FLAG            0x08 // флаг разрешения постоянного хранения ошибок
#define CAR_SPEED_SENSOR_FLAG       0x10 // флаг комплектации датчиком скорости автомобиля
#define SYNCHRONOUS_INJECTION_FLAG  0x20 // флаг разрешения одновременного впрыска
#define ASYNCHRONOUS_INJ_ACCEL_FLAG 0x40 // флаг разрешения асинхронного впрыска при ускорении
// Описание слова режима работы 1.
#define ENGINE_OFF_FLAG             0x01 // признак выключения двигателя
#define IDLE_FLAG                   0x02 // признак холостого хода
#define ENRICHMENT_BY_POWER_FLAG    0x04 // признак обгащения по мощности
#define FUEL_SUPPLY_BLOCKING_FLAG   0x08 // признак блокировки подачи топлива
#define OXYGEN_SENSOR_REGUL_FLAG    0x10 // признак зоны регулирования по датчику кислорода
#define DETONATION_ZONE_FLAG        0x20 // признак попадания в зону детонации
#define ADSORBER_REFRESH_FLAG       0x40 // признак разрешения продувки адсорбера
#define SAVE_OXY_SENS_RESULT_FLAG   0x80 // признак сохранения результатов обучения по датчику кислорода
// Описание слова режима работы 2.
#define IDLE_PREV_FLAG              0x02 // признак наличия холостого хода в прошлом цикле вычислений
#define IDLE_BLOCK_FLAG             0x04 // разрешение блокировки выхода из режима холостого хода
#define DETONATION_PREV_FLAG        0x08 // признак попадания в зону детонации в прошлом цикле вычислений
#define ADSORBER_REFRESH_PREV_FLAG  0x10 // признак наличия продувки адсорбера в прошлом цикле вычислений
#define DETONATION_FLAG             0x20 // признак обнаружения детонации
#define PREV_OXYGEN_SENS_STATE_FLAG 0x40 // признак прошлого состояния датчика кислорода
#define CURR_OXYGEN_SENS_STATE_FLAG 0x80 // признак текущего состояния датчика кислорода
// Описание слова флагов текущих неисправностей 1.
#define CRANKSHAFT_SYNC_ERROR_FLAG  0x01 // ошибка датчика синхронизации КВ
#define EEPROM_ERROR_FLAG           0x04 // ошибка EEPROM
#define HEATER_OXY_SENSOR_ERR_FLAG  0x08 // ошибка нагревателя датчика кислорода
#define PHASE_SENSOR_ERROR_FLAG     0x10 // ошибка датчика фазы
#define CPU_RESET_ERROR_FLAG        0x20 // ошибка сброса процессора
#define RAM_ERROR_FLAG              0x40 // ошибка ОЗУ
#define ROM_ERROR_FLAG              0x80 // ошибка ПЗУ
// Описание слова флагов текущих неисправностей 2.
#define LOW_SYSTEM_VOLTAGE_ERR_FLAG 0x01 // низкое бортовое напряжение
#define LOW_SIG_TEMP_SENS_ERR_FLAG  0x08 // низкий уровень сигнала с датчика темп. охл. жидкости
#define LOW_SIG_OXY_SENS_ERR_FLAG   0x10 // низкий уровень сигнала с кислородного датчика
#define LOW_SIG_THROTTLE_ERR_FLAG   0x20 // низкий уровень сигнала с датчика положения дросселя
#define LOW_SIG_AIR_FLOW_ERR_FLAG   0x40 // низкий уровень сигнала с датчика расхода воздуха
#define LOW_ENGINE_NOISE_ERR_FLAG   0x80 // низкий уровень шума двигателя
// Описание слова флагов текущих неисправностей 3.
#define HI_SYSTEM_VOLTAGE_ERR_FLAG  0x01 // высокое бортовое напряжение
#define HIGH_SIG_TEMP_SENS_ERR_FLAG 0x08 // высокий уровень сигнала с датчика темп. охл. жидкости
#define HIGH_SIG_OXY_SENS_ERR_FLAG  0x10 // высокий уровень сигнала с кислородного датчика
#define HIGH_SIG_THROTTLE_ERR_FLAG  0x20 // высокий уровень сигнала с датчика положения дросселя
#define HIGH_SIG_AIR_FLOW_ERR_FLAG  0x40 // высокий уровень сигнала с датчика расхода воздуха
#define HIGH_ENGINE_NOISE_ERR_FLAG  0x80 // высокий уровень шума двигателя
// Описание слова флагов текущих неисправностей 4.
#define CNOCK_SENS_DISCON_ERR_FLAG  0x01 // обрыв датчика детонации
#define IMMOBILIZER_CONN_ERR_FLAG   0x02 // нет связи с иммобилизатором
#define OXY_SENSOR_ACTIVE_ERR_FLAG  0x08 // нет активности датчика кислорода
#define OXY_SENS_LEANING_ERR_FLAG   0x10 // нет отклика датчика кислорода при обеднении
#define OXY_SENS_ENRICHM_ERR_FLAG   0x20 // нет отклика датчика кислорода при обогащении
#define CAR_SPEED_SENSOR_ERR_FLAG   0x40 // ошибка датчика скорости автомобиля
#define STEP_MOTOR_ERR_FLAG         0x80 // ошибка регулятора холостого хода
// Описание флагов состояния датчика кислорода.
#define OXYGEN_SENSOR_READY_FLAG    0x01 // флаг готовности датчика кислорода
#define OXY_HEAT_PERMISSION_FLAG    0x02 // флаг разрешения нагрева датчика кислорода
/* Для RLI_EOL (0x02) */
#define CONFIGURATION1_POS              3  // Слово комплектации 1
#define CONFIGURATION2_POS              4  // Слово комплектации 2
#define WORK_MODE1_POS                  5  // Слово режима работы 1
#define WORK_MODE2_POS                  6  // Слово режима работы 2
#define FREESE_TEMP_POS                 7 // N=E-40 [°C]
#define AIR_FUEL_RATIO_POS              8 // N=14.7*(E+128)/256
#define THROTTLE_POS_POS                9 // N=E [%]
#define ENGINE_SPEED_POS                10 // N=E*40 [об/мин]
#define IDLE_ENGINE_SPEED_POS           11 // N=E*10 [об/мин]
#define CURRENT_STEP_POS                12 // N=E [шагов]
#define INJECTOR_TIME_CORR_POS          13 // N=(E+128)/256
#define SPARK_ADVANCE_POS               14 // N=E/2 [гр.КВ], где E-знаковое
#define CAR_SPEED_POS                   15 // N=E [км/час]
#define SYSTEM_VOLTAGE_POS              16 // N=5.2 + E*0.05 [В]
#define OXYGEN_SENSOR_FLAGS_POS         17 // Флаги состояния датчика кислорода
#define INJECTION_TIME_LSB_POS          18 // Длительность импульса впрыска
#define INJECTION_TIME_MSB_POS          19 // N=E/125 [мсек]
#define AIR_FLOW_LSB_POS                20 // Массовый расход воздуха (младший байт)
#define AIR_FLOW_MSB_POS                21 // N=E/10 [кг/час]
#define CYCLE_AIR_FLOW_LSB_POS          22 // Цикловой расход воздуха (младший байт)
#define CYCLE_AIR_FLOW_MSB_POS          23 // N=E/6 [мг/такт]
#define HOUR_FUEL_CONSUMPTION_LSB_POS   24 // Часовой расход топлива (младший байт)
#define HOUR_FUEL_CONSUMPTION_MSB_POS   25 // N=E/50[л/час]
#define TRAVEL_FUEL_CONSUMPTION_LSB_POS 26 // Путевой расход топлива(младший байт)
#define TRAVEL_FUEL_CONSUMPTION_MSB_POS 27 // N=E/128[л/100км]
/* Для RLI_FT (0x03) */
#define CNOCK_SENSOR_ADC_DATA           3  // N=E*5.0/256 [В]
#define FREEZE_TEMP_SENSOR_ADC_DATA     4  // N=E*5.0/256 [В]
#define AIR_FLOW_SENSOR_ADC_DATA        5  // N=E*5.0/256 [В]
#define SYSTEM_VOLTAGE_ADC_DATA         6  // N=0.287*E*5.0/256 [В]
#define OXYGEN_SENSOR_ADC_DATA          7  // N=E*5.0/256 [В]
#define THROTTLE_SENSOR_ADC_DATA        8  // N=E*5.0/256 [В]
#define MCU_PORT1_STATE                 9  // Состояние порта 1 микроконтроллера
#define MCU_PORT5_STATE                 10 // Состояние порта 5 микроконтроллера
#define MCU_PORT6_STATE                 11 // Состояние порта 6 микроконтроллера
#define MCU_PORT8_STATE                 12 // Состояние порта 8 микроконтроллера
// Состояние порта 1 микроконтроллера.
#define DETONATION_INTEGRATOR           0x02
#define COOLING_FAN                     0x80
// Состояние порта 5 микроконтроллера.
#define FUEL_PUMP_RELAY                 0x02
#define CONTROL_LAMP                    0x08
#define CONDITIONER_RELAY               0x10
// Состояние порта 6 микроконтроллера.
#define DETONATION_GAIN_0BIT            0x01
#define DETONATION_GAIN_2BIT            0x02
#define DETONATION_GAIN_1BIT            0x04
// Состояние порта 8 микроконтроллера.
#define CONDITIONER_IN                  0x02
#define L_LINE_INPUT                    0x04
/* Для RLI_IR (0x04) */
#define IMMOBILIZER_STATE1              3
#define IMMOBILIZER_STATE2              4
#define SYSTEM_ID1                      5
#define SYSTEM_ID2                      6
// Слово состояния функций иммобилизации 2.
#define IMMOBILIZER_CONN_ERR_FLAG       0x10
#define IMMOBILIZER_RUN_FLAG            0x20
#define IMMOBILIZER_PROGRAMMING_FLAG    0x40
#define IMMOBILIZER_ACTIVATED_FLAG      0x80

/* Read memory by address (RMBA == 0x23) */
#define MT_XRAM         0x00   // memoryType, ExternalMemoryType
#define MEMORY_ADDR_MIN 0xfd00 // memoryAddress
#define MEMORY_ADDR_MAX 0xfeff
#define MS_MAX          0x78   // MemorySize, максимальный размер

/* Write data by local identifier (WDBLI == 0x3b) */
// Параметр recordLocalIdentifier
#define RLI_VIN         0x90 // vehicleIdentificationNumber
#define RLI_RSC         0x98 // repairShopCode
#define RLI_BSN         0xa1 // bodySerialNumber
#define RLI_ESN         0xa2 // engineSerialNumber
#define RLI_MD          0xa3 // manufacturerDate
#define RLI_VIN_SIZE    19
#define RLI_RSC_SIZE    7
#define RLI_BSN_SIZE    7
#define RLI_ESN_SIZE    7
#define RLI_MD_SIZE     10

/* Input output control by local identifier (IOCBLI == 0x30) */
// Параметр inputOutputLocalIdentifier
#define I1OC    0x01 // injector1OutputControl прямое управление форсункой 1
#define I2OC    0x02 // injector2OutputControl прямое управление форсункой 2
#define I3OC    0x03 // injector3OutputControl прямое управление форсункой 3
#define I4OC    0x04 // injector4OutputControl прямое управление форсункой 4
#define IGN1OC  0x05 // ignition1OutputControl управление катушкой 1и4 цилиндров
#define IGN2OC  0x06 // ignition2OutputControl управление катушкой 2и3 цилиндров
#define FPROC   0x09 // fuelPumpRelayOutputControl управление реле бензонасоса
#define CSFROC  0x0a // coolingSytemFanRelayOutputControl реле вентилятора системы охлаждения
#define ACROC   0x0b // airConditionRelayOutputControl управление реле кондиционера
#define MILOC   0x0c // malfunctionIndicationLampOutputControl упр. лампой check
#define CPVOC   0x0d // canisterPurgeValveOutputControl клапаном продувки адсорбера
#define ISMPA   0x41 // idleStepMotorPositionAdjustment пол. регулятора холостого хода
#define IESA    0x42 // idleEngineSpeedAdjustment управление оборотами холостого хода
// параметр inputOutputControlParameter
#define RCTECU  0x00 // returnControlToECU тестер больше не управляет входом/выходом
#define RCS     0x01 // reportCurrentState запрашивает текущее состояние
#define RIOC    0x02 // reportIOConditions запрашивает условия для управления
#define RIOS    0x03 // reportIOScaling запрашивает способ масштабирования входа/выхода
#define RTD     0x04 // resetToDefault запрашивает сброс входа/выхода
#define FCS     0x05 // freezeCurrentState “заморозить” текущее состояние входа/выхода
#define ECO     0x06 // executeControlOption запрашивает выполнить действие controlOption.
#define STA     0x07 // shortTermAdjustment ОЗУ до значения в controlOption.
#define LTA     0x08 // longTermAdjustment EEPROM до значения в controlOption.
#define RIOCP   0x09 // reportIOCalibrationParameters запрашивает калибровочные данные
// все параметры можно сбросить, запросить состояние, запросить действие или корректировку (для рхх)

/* Коды ошибок */
#define P0102   0x0102 // Низкий уровень сигнала с датчика расхода воздуха
#define P0103   0x0103 // Высокий уровень сигнала с датчика расхода воздуха
#define P0117   0x0117 // Низкий уровень сигнала с датчика температуры охлаждающей жидкости
#define P0118   0x0118 // Высокий уровень сигнала с датчика температуры охлаждающей жидкости
#define P0122   0x0122 // Низкий уровень сигнала с датчика положения дросселя
#define P0123   0x0123 // Высокий уровень сигнала с датчика положения дросселя
#define P0131   0x0131 // Низкий уровень сигнала с датчика кислорода
#define P0132   0x0132 // Высокий уровень сигнала с датчика кислорода
#define P0134   0x0134 // Нет активности датчика кислорода
#define P0135   0x0135 // Обрыв нагревателя датчика кислорода
#define P0171   0x0171 // Система слишком бедная
#define P0172   0x0172 // Система слишком богатая
#define P0201   0x0201 // Цепь управления форсункой No1, обрыв
#define P0202   0x0202 // Цепь управления форсункой No2, обрыв
#define P0203   0x0203 // Цепь управления форсункой No3, обрыв
#define P0204   0x0204 // Цепь управления форсункой No4, обрыв
#define P0261   0x0261 // Цепь управления форсункой No1, замкнута на землю
#define P0262   0x0262 // Цепь управления форсункой No1, замкнута на +12В
#define P0264   0x0264 // Цепь управления форсункой No2, замкнута на землю
#define P0265   0x0265 // Цепь управления форсункой No2, замкнута на +12В
#define P0267   0x0267 // Цепь управления форсункой No3, замкнута на землю
#define P0268   0x0268 // Цепь управления форсункой No3, замкнута на +12В
#define P0270   0x0270 // Цепь управления форсункой No4, замкнута на землю
#define P0271   0x0271 // Цепь управления форсункой No4, замкнута на +12В
#define P0325   0x0325 // Обрыв датчика детонации
#define P0327   0x0327 // Низкий уровень шума двигателя
#define P0328   0x0328 // Высокий уровень шума двигателя
#define P0335   0x0335 // Ошибка датчика синхронизации КВ
#define P0340   0x0340 // Ошибка датчика фазы
#define P0443   0x0443 // Неисправность управления клапаном продувки адсорбера
#define P0480   0x0480 // Неисправность цепи управления вентилятором No1
#define P0501   0x0501 // Ошибка датчика скорости автомобиля
#define P0505   0x0505 // Ошибка регулятора холостого хода
#define P0562   0x0562 // Низкое бортовое напряжение
#define P0563   0x0563 // Высокое бортовое напряжение
#define P0601   0x0601 // Ошибка ПЗУ
#define P0603   0x0603 // Ошибка ОЗУ
#define P1410   0x1410 // Цепь управления клапаном продувки адсорбера, замыкание на +12В
#define P1425   0x1425 // Цепь управления клапаном продувки адсорбера, замыкание на землю
#define P1426   0x1426 // Цепь управления клапаном продувки адсорбера, обрыв
#define P1501   0x1501 // Цепь управления реле бензонасоса, замыкание на землю
#define P1502   0x1502 // Цепь управления реле бензонасоса, замыкание на +12В
#define P1509   0x1509 // Цепь управления регулятором холостого хода, перегрузка
#define P1513   0x1513 // Цепь управления регулятором холостого хода, замыкание на землю
#define P1514   0x1514 // Цепь управления регулятором холостого хода, обрыв или замыкание на +12В
#define P1541   0x1541 // Цепь управления реле бензонасоса, обрыв
#define P1600   0x1600 // Нет связи с иммобилизатором
#define P1602   0x1602 // Пропадание напряжения бортсети
#define P1603   0x1603 // Ошибка EEPROM
#define P1612   0x1612 // Ошибка сброса блока управления

/* Тайминги в милисекундах */
#define P1MIN   0       // Межбайтовый интервал для ответа блока управления
#define P1MAX   20
#define P2MIN   25      // Время между запросом тестера и ответом блока управления
#define P2MAX   50
#define P3MIN   100     // Время между окончанием ответа блока управления и началом следующего запроса
#define P3MAX   5000
#define P4MIN   0       // Межбайтовый интервал для запроса диагностического тестера
#define P4MAX   20

#endif
